import 'package:annualproject/view/onStart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

void main() async {
  await DotEnv().load('.env');
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'InfoSearch',
      debugShowCheckedModeBanner: false,
      home: OnstartPage(),
    );
  }
}
