class UserDetails {

  String _userName = "";
  String _password = "";
  String _name = "";
  int _age = 0;
  String _email = "";
  String _phone = "";
  DateTime _birthday = null;

  UserDetails(this._userName, this._password, this._name, this._age, this._email, this._phone, this._birthday);

  String get userName {
    return _userName;
  }

  void set userName(String newUserName){
    _userName = newUserName;
  }

  void set password(String newPassword){
    _password = newPassword;
  }

  String get name {
    return _name;
  }

  void set name(String newName){
    _name = newName;
  }

  int get age {
    return _age;
  }

  void set age(int newAge) {
    _age = newAge;
  }

  String get email {
    return _email;
  }

  void set email(String newEmail) {
    _email = newEmail;
  }

  String get phone {
    return _phone;
  }

  void set phone(String newPhone){
    _phone = newPhone;
  }

  DateTime get birthday {
    return _birthday;
  }

  void set birthday(DateTime newBirthday) {
    _birthday = newBirthday;
  }

}