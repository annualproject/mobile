class HistoryItem {

  String _documentSetUuid = "";
  String _documentSetName = "";
  int _documentSetLength = 0;
  String _queryString = "";
  String _taskStatus = "";
  String _taskSynthesis = "";

  HistoryItem(this._documentSetUuid, this._documentSetName, this._documentSetLength, this._queryString, this._taskStatus, this._taskSynthesis);

  String get documentSetUuid {
    return _documentSetUuid;
  }

  String get documentSetName {
    return _documentSetName;
  }

  void set documentSetName(String newName) {
    _documentSetName = newName;
  }

  int get documentSetLength {
    return _documentSetLength;
  }

  void set documentSetLength(int newLength) {
    _documentSetLength = newLength;
  }

  String get queryString {
    return _queryString;
  }

  String get taskStatus {
    return _taskStatus;
  }

  void set taskStatus(String newStatus) {
    _taskStatus = newStatus;
  }

  String get taskSynthesis {
    return _taskSynthesis;
  }
}