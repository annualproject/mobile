
import 'package:annualproject/view/profile_update.dart';
import 'package:flutter/material.dart';

class ProfileAddCardWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Card(
          child:IconButton(
            icon: Icon (Icons.edit),
           color: Color(0xFF27cf53),

              onPressed: (){
                Navigator.of(context).push(MaterialPageRoute
                  (builder: (BuildContext context) =>
                    ProfileUpdate()
                ));
              },
            )
          )
        ],
      ),
    );
  }
}


class ProfileCardWidget extends StatelessWidget {
  final String title;
  final String desc;
  final String secondTitle;
  final String secondDesc;
  final int integer;


  final IconData icon;
  final Color iconColor;

  const ProfileCardWidget(
      {Key key, this.title, this.desc, this.secondTitle, this.secondDesc, this.icon, this.iconColor, this.integer })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: AppTheme.primary,
      elevation: 0.0,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Icon(icon, color: iconColor),
            SizedBox(height: 25.0),
            Text(
              '$title',
              style: AppTheme.cardTitleStyle(context),
            ),
            Text(
              '$desc',
              style: AppTheme.cardDescStyle(context),
            ),
            secondTitle != null ?
            Text(
              '$secondTitle',
              style: AppTheme.cardTitleStyle(context),
            ): Container(),
            secondDesc != null ?
            Text(
              '$secondDesc',
              style: AppTheme.cardDescStyle(context),
            ): Container(),
          ],
        ),
      ),
    );
  }
}

class ProfileHeaderWidget extends StatelessWidget {
  final String title;
  final String desc;
  final String secondTitle;
  final String secondDesc;

  const ProfileHeaderWidget({Key key, this.title, this.desc, this.secondTitle, this.secondDesc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 10.0,
        ),
        SizedBox(
          height: 10.0,
        ),
        
      ],
    );
  }
}

class AppTheme {
  static final Color primary = Color(0xFF37334d);
  static final Color cardColor = Color(0xFF3353d);
  static final Color addCardColor = Color(0xFF4863DB);
  static final List<Color> iconColors = [
    Colors.blue,
    Color(0xFF27cf53),
    Color(0xFFf3a643),
    Color(0xFFbb49dc),
    Color(0xFF03DAC5)
  ];

  static TextStyle profileHeaderStyle(BuildContext context) => Theme.of(context)
      .textTheme
      .headline
      .copyWith(color: Colors.white, fontWeight: FontWeight.bold);

  static TextStyle cardTitleStyle(BuildContext context) => Theme.of(context)
      .textTheme
      .subhead
      .copyWith(color: Colors.white, fontWeight: FontWeight.bold);

  static TextStyle cardDescStyle(BuildContext context) =>
      Theme.of(context).textTheme.subhead.copyWith(color: Colors.grey);
}

class ProfileImageWidget extends StatelessWidget {
  final String image ="assets/prof.png";

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40.0,
      margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 20.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(50.0),
          bottomRight: Radius.circular(50.0),
          topLeft: Radius.circular(50.0),
        ),
        image: DecorationImage(
          image: AssetImage(
            image,
          ),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
