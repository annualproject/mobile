import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http_logger/log_level.dart';
import 'package:http_logger/logging_middleware.dart';
import 'package:http_middleware/http_with_middleware.dart';
import 'package:shared_preferences/shared_preferences.dart';

HttpWithMiddleware httpClient = HttpWithMiddleware.build(middlewares: [
  HttpLogger(logLevel: LogLevel.BODY),
]);

var urlApi = DotEnv().env['URL_API'];

Future addTaskSearch(String searchQuery) async{
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

  Map data = {
    'searchQuery': searchQuery,
  };

  var response;

  try {
    //10.0.2.2 for android emulator
    response = await httpClient.post(urlApi + "search",
        headers: {
          "Content-type": "application/json",
          'Authorization': 'Bearer ' + sharedPreferences.get("token")
        }, body: json.encode(data));
  }
  catch (e) {
    return 500;
  }

  if (response.statusCode == 200) {
    return 200;
  }
  else if(response.statusCode == 409){
    return 409;
  }
  else {
    return 500;
  }
}

Future getSearchHistoryOfUser() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

  var response;

  try{
    //10.0.2.2 for android emulator
    print(sharedPreferences.get("token"));
    response = await httpClient.get( urlApi + "taskSearch/user/" + sharedPreferences.get("userId"),
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch( e ){
    print(e.toString());
    //_showDialogErreur("Connexion au serveur impossible");
    return null;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){
      var r = jsonDecode(response.body);
      return r.reversed.toList();
    }
    return null;
  }
  else if(response.statusCode == 400 || response.statusCode == 401 ) {
    var res = json.decode(response.body);
    print(res["message"]);
    //_showDialogErreur(res["message"]);
  }
  else {
    print(response.body);
  }
}