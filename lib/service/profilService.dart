import 'dart:convert';

import 'package:annualproject/model/UserDetails.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http_logger/log_level.dart';
import 'package:http_logger/logging_middleware.dart';
import 'package:http_middleware/http_with_middleware.dart';
import 'package:shared_preferences/shared_preferences.dart';


HttpWithMiddleware httpClient = HttpWithMiddleware.build(middlewares: [
  HttpLogger(logLevel: LogLevel.BODY),
]);

var urlApi = DotEnv().env['URL_API'];

Future<UserDetails> getUserDetails() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var response;
  try {
    //10.0.2.2 for android emulator
    response = await httpClient.get(urlApi + "user/information",
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});

  } catch (e) {
    return null;
  }
  if(response.statusCode == 200) {
    print(jsonDecode(response.body));
    var r =  jsonDecode(response.body);

    var birthday = r["birthday"] != null ?  DateTime.parse(r["birthday"]) : null;
    return new UserDetails(r["username"], "", r["name"], r["age"], r["email"], r["phone"], birthday);
  }
  else if(response.statusCode == 400 || response.statusCode == 401 ) {
    var res = json.decode(response.body);
    print(res["message"]);
  }

}

updateUser(String name, int age, String phone, String birthday, String email, String password) async {

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  Map data = {
    'name': name,
    'age': age,
    'email': email,
    'phone': phone,
    'birthday': birthday,
    'password' : password
  };

  var response;
  try{
    response = await httpClient.put( urlApi + "user/"+sharedPreferences.get("userId"),
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")},
        body:json.encode(data));
  }
  catch( e ){
    return 500;
  }
  if(response.statusCode == 200) {
    return 200;
  }
  else if(response.statusCode == 409){
    return 409;
  }
  else {
    return 500;
  }
}
