import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http_logger/log_level.dart';
import 'package:http_logger/logging_middleware.dart';
import 'package:http_middleware/http_with_middleware.dart';
import 'package:shared_preferences/shared_preferences.dart';

HttpWithMiddleware httpClient = HttpWithMiddleware.build(middlewares: [
  HttpLogger(logLevel: LogLevel.BODY),
]);

var urlApi = DotEnv().env['URL_API'];

Future getDatasetOfUser() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var response;
  try{
    response = await httpClient.get( urlApi + "documentSets/userSets",
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch( e ){
    return null;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){
      var r =  jsonDecode(response.body);
      return r.reversed.toList();
    }
    else{
      return null;
    }

  }
  else if(response.statusCode == 400 || response.statusCode == 401 ) {
    var res = json.decode(response.body);
    //print(res["message"]);
    //_showDialogErreur(res["message"]);
  }
  else {
    //print(response.body);
  }
}

Future getDocumentSetByUuid(String documentSetUuid) async {

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

  var response;

  try {
    //10.0.2.2 for android emulator
    response = await httpClient.get( urlApi + "documentSets/" + documentSetUuid,
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch( e ){
    print(e.toString());
    return 500;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){
      var documentSet =  jsonDecode(response.body);
      return documentSet;
    }
    else{
      return "";
    }
  }
  else{
    return 500;
  }
}


Future getDocumentsOfDocumentSet(String documentSetUuid) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var response;
  try{
    response = await httpClient.get( urlApi + 'documentSets/'+ documentSetUuid +'/documents',
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch( e ){
    print(e.toString());
    return null;
  }
  if(response.statusCode == 200 || response.statusCode == 200) {
    var r =  jsonDecode(response.body);
    return r;
  }
  else if(response.statusCode == 400 || response.statusCode == 401 ) {
    var res = json.decode(response.body);
    print(res["message"]);

  }
  else {
    print(response.body);
  }
}

Future deleteDocument(String documentSetUuid , String documentUuid) async{
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var response;
  try{
    response = await httpClient.delete(urlApi + 'documentSets/' + documentSetUuid + '/documents/' + documentUuid,
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});

  } catch( e ) {
    print(e.toString());
  }
  if(response.statusCode == 200 || response.statusCode == 201 ) {
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
  }
  else if(response.statusCode == 400 || response.statusCode == 401 ) {
    var res = json.decode(response.body);
    print(res["message"]);

  }
  else {
    print(response.body);
  }
}

