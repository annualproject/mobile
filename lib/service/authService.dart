import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http_logger/log_level.dart';
import 'package:http_logger/logging_middleware.dart';
import 'package:http_middleware/http_with_middleware.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:shared_preferences/shared_preferences.dart';

HttpWithMiddleware httpClient = HttpWithMiddleware.build(middlewares: [
  HttpLogger(logLevel: LogLevel.BODY),
]);

var urlApi = DotEnv().env['URL_API'];

auth(String username, String password) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var jsonResponse = null;

  Map data = {
    'username': username,
    'password': password
  };

  var response;
  try{
    //10.0.2.2 for android emulator
    response = await httpClient.post( urlApi + "auth",
        headers: {"Content-type": "application/json"}, body:json.encode(data));
  }
  catch( e ){
    print(e.toString());
    return 500;
  }

  if(response.statusCode == 200) {
    jsonResponse = json.decode(response.body);

    if(jsonResponse != null) {
      sharedPreferences.setString("token", jsonResponse['jwt']);
      Map<String, dynamic> decodedToken = JwtDecoder.decode(jsonResponse['jwt']);
      sharedPreferences.setString("userId", decodedToken['id']);
    }
    return 200;
  }
  else if(response.statusCode == 403) {
    return 403;
  }
  else {
    return 500;
  }
}

register(String email, String username, String password) async {

  Map data = {
    "email": email,
    'username': username,
    'password': password
  };

  var response = await httpClient.post( urlApi + "user",
      headers: {"Content-type": "application/json"}, body:json.encode(data));


  if(response.statusCode == 201) {
    return 201;
  }
  else if(response.statusCode == 400) {
    return 400;
  }
  else if(response.statusCode == 409) {
    return 409;
  }
  else {
    return 500;
  }
}