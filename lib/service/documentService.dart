import 'dart:io';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http_parser/http_parser.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

var urlApi = DotEnv().env['URL_API'];

Future downloadFile( String filename, String uuid) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  http.Client client = new http.Client();
  var req = await client.get(Uri.parse( urlApi + "documents/" + uuid),
      headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  var bytes = req.bodyBytes;
  String dir = (await getApplicationDocumentsDirectory()).path;
  File file = new File('$dir/$filename');

  await file.writeAsBytes(bytes);

  if( req.statusCode == 200 ||  req.statusCode == 201){
    return file.path;
  }
  else{
    return 500;
  }
}


Future uploadfile(String fileUrl, String documentSetUuid) async {

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  try {
    var uri = Uri.parse(urlApi + 'documents');
    var request = http.MultipartRequest('POST', uri)
      ..files.add(await http.MultipartFile.fromPath(
          'mFile', fileUrl,
          contentType: MediaType('application', 'x-tar')));

    request.headers['authorization'] =
        'Bearer ' + sharedPreferences.get("token");

    var response = await request.send();
    if (response.statusCode == 201){
      var documentUuid = response.headers["location"].split("/")[4];

      var resultDocumentSet;
      try{
        resultDocumentSet = await http.put(urlApi + 'documentSets/' + documentSetUuid + '/documents/' + documentUuid,
            headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
        if(resultDocumentSet.statusCode == 200 || resultDocumentSet.statusCode == 201) {
          return 201;
        }
        else{
          return 500;
        }

      } catch( e ) {
        return 500;
      }
    }
    else{
      return 500;
    }

  } catch (e) {
    return 500;
  }
}