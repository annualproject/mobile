import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http_logger/log_level.dart';
import 'package:http_logger/logging_middleware.dart';
import 'package:http_middleware/http_with_middleware.dart';
import 'package:shared_preferences/shared_preferences.dart';

HttpWithMiddleware httpClient = HttpWithMiddleware.build(middlewares: [
  HttpLogger(logLevel: LogLevel.BODY),
]);

var urlApi = DotEnv().env['URL_API'];

Future addTaskAnalyse(String searchQuery, selectedDocumentSet, bool summaryChecked, int summarySize) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

  Map data = {
    'queryString': searchQuery,
    'documentSummary': summaryChecked,
    'documentSetUuid': selectedDocumentSet["uuid"],
    'summarySize': summarySize
  };

  var response;
  try {
    //10.0.2.2 for android emulator
    response = await httpClient.post(urlApi + "taskAnalyse/",
        headers: {
          "Content-type": "application/json",
          'Authorization': 'Bearer ' + sharedPreferences.get("token")
        }, body: json.encode(data));
  }
  catch (e) {
    return 500;
  }

  if (response.statusCode == 201) {
    return response.headers['location'];
  }
  else {
    return 500;
  }
}

Future getCurrentTaskAnalyse(String taskAnalyseUrl) async{
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

  var response;

  try {
    //10.0.2.2 for android emulator
    response = await httpClient.get(taskAnalyseUrl,
        headers: {
          "Content-type": "application/json",
          'Authorization': 'Bearer ' + sharedPreferences.get("token")
        });
  }
  catch (e) {
    return 500;
  }

  if (response.statusCode == 200) {
    var res = json.decode(utf8.decode(response.bodyBytes));
    return res;
  }
  else {
    return 500;
  }
}

Future getTaskAnalysisOfUser() async {

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

  var response;

  try{
    //10.0.2.2 for android emulator
    response = await httpClient.get( urlApi + "taskAnalyse/user/" + sharedPreferences.get("userId"),
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch( e ){
    return 500;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){
      var result = json.decode(utf8.decode(response.bodyBytes));
      var listResult = result.toList();

      listResult.sort((a,b) => DateTime.parse(a["createdAt"]).compareTo(DateTime.parse(b["createdAt"])));
      return listResult.reversed;
    }
    return [];
  }
  else {
    return 500;
  }
}
