
import 'package:annualproject/model/UserDetails.dart';
import 'package:annualproject/service/profilService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:email_validator/email_validator.dart';
import 'package:validators/validators.dart';

class ProfileUpdate extends StatefulWidget {
  @override
  _ProfileUpdateState createState() => _ProfileUpdateState();
}

class _ProfileUpdateState extends State<ProfileUpdate> {
  UserDetails _userDetails;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserDetails().then((value) => {
      setState((){
        _userDetails = value;
      })
    });
  }

  final TextEditingController usernameController = new TextEditingController();
  final TextEditingController nameController = new TextEditingController();
  final TextEditingController ageController = new TextEditingController();
  final TextEditingController birthdayController = new TextEditingController();
  final TextEditingController phoneController = new TextEditingController();
  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();

  bool _isLoading = false;

  updateUserInfo(String name, String age, String phone, DateTime birthday, String email, String password) async {

    if(email.isEmpty){
      _showDialog("Champs invalides", "Vous devez spécifier un email", 1);
      return;
    }
    if(!EmailValidator.validate(email)){
      _showDialog("Champs invalides", "Votre email n'est pas valide", 1);
      return;
    }
    if(!isNumeric(phone)){
      _showDialog("Champs invalides", "Votre numéro de téléphone n'est pas valide", 1);
      return;
    }
    if(!isAlpha(name)){
      _showDialog("Champs invalides", "Votre nom ne doit contenir que des lettres", 1);
      return;
    }
    if(!age.isEmpty) {
      try {
        int.parse(age);
      }
      catch (e) {
        _showDialog("Champs invalides", "Votre age doit être un nombre", 1);
        return;
      }
    }

    int ageNumber = !age.isEmpty ? int.parse(age) : 0;
    String birthdayDate = birthday != null ? birthday.toIso8601String().split('T')[0] : "";

    setState(() {
      _isLoading = true;
    });
    var result = await updateUser(name, ageNumber, phone, birthdayDate, email, password);

    if(result == 200){
      _showDialog("Mise à jour", "Vos modifications ont bien été prises en comptes", 3);
    }
    else if(result == 409){
      _showDialog("Erreur", "Votre nouveau nom d'utilisateur est déjà utilisé par une autre personne", 1);
    }
    else{
      _showDialog("Oups", "Une erreur s'est produite", 1);
    }
    setState(() {
      _isLoading = false;
    });

  }

  String formatDate(DateTime date){
    var day = date.day < 10 ?  '0' + date.day.toString() : date.day.toString();
    var month = date.month < 10 ?  '0' + date.month.toString() : date.month.toString();
    return  day + "-" + month + "-" +
    date.year.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        backgroundColor: Color(0xFF4563DB),
        title: Text("Modification",
          style: TextStyle(
            color: Colors.white,
            fontSize: 17,
            fontFamily: "Montserrat",
          ),
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          icon: Icon (Icons.arrow_back_ios),
          color: Colors.white,
        ),
        elevation: 0.0,
      ),

        body:
        SingleChildScrollView(
          child:
        Container(
          width: double.maxFinite,
          color: Color(0xFF37334d),
          child: Stack(
            children: <Widget>[
              Positioned(
                child: Container(
                  padding: EdgeInsets.all(32),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                      color: Colors.white
                  ),
                  child: _userDetails != null ? Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 16, bottom: 0),
                        child: TextFormField(
                          enabled: false,
                          controller: usernameController..text = _userDetails.userName,
                          decoration: InputDecoration(
                            hintText: 'Nom d\'utilisateur *',
                            icon: const Icon(Icons.people),
                          ),

                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 16, bottom: 0),
                        child: TextFormField(
                          controller: nameController..text = _userDetails.name,
                          decoration: InputDecoration(
                            hintText: 'Nom',
                            icon: const Icon(Icons.person),
                          ),

                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 16, bottom: 0),
                        child: TextFormField(
                          controller: ageController..text = _userDetails.age != 0 ?
                          _userDetails.age.toString() : null,
                          decoration: InputDecoration(
                            hintText: 'Age',
                            icon: const Icon(Icons.broken_image),
                          ),

                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 16, bottom: 16),
                        child: TextFormField(
                          controller: emailController..text = _userDetails.email,
                          decoration: InputDecoration(
                            hintText: 'Email *',
                            icon: const Icon(Icons.email),
                          ),
                        ),
                      ),

                      Row(
                        children: <Widget>[
                          Expanded(
                            child:
                            TextFormField(
                              enabled: false,
                              controller: birthdayController..text = _userDetails.birthday != null ?
                              formatDate(_userDetails.birthday) : null,
                              decoration: InputDecoration(
                                hintText: 'Date de naissance',
                                icon: const Icon(Icons.calendar_today),
                              ),
                            ),
                          ),
                          FlatButton(
                              onPressed: () {
                                DatePicker.showDatePicker(context,
                                    showTitleActions: true,
                                    minTime: DateTime(1900, 1, 1),
                                    maxTime: DateTime.now(),
                                    onChanged: (date) {},
                                    onConfirm: (date) {
                                      setState(() {
                                        _userDetails.birthday = date;
                                      });
                                    },
                                    currentTime: DateTime.now(), locale: LocaleType.fr);
                              },
                              child: Icon(
                                Icons.edit,
                                color: Colors.blue,
                              )
                          )
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20, bottom: 30),
                        child: TextFormField(
                          controller: passwordController,
                          decoration: InputDecoration(
                            hintText: 'Mot de passe',
                            icon: const Icon(Icons.lock),
                          ),
                          obscureText: true,
                        ),
                      ),
                      InternationalPhoneNumberInput(
                        onInputChanged: (PhoneNumber number) {
                          print(number.phoneNumber);
                        },
                        onInputValidated: (bool value) {
                          print(value);
                        },
                        countries: ['FR'],
                        ignoreBlank: false,
                        autoValidate: false,
                        hintText: "Numéro de téléphone",
                        selectorTextStyle: TextStyle(color: Colors.black),
                        initialValue:  PhoneNumber(isoCode: 'FR'),
                        textFieldController: phoneController..text = _userDetails.phone != null ?
                        _userDetails.phone : "",
                        inputBorder: OutlineInputBorder(),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 30.0, bottom: 30.0),
                        height: 45,
                        width: double.maxFinite,
                        decoration: BoxDecoration(
                          color: Color(0xFF4563DB),
                          borderRadius: BorderRadius.all(
                              Radius.circular(32)
                          ),
                        ),
                        child: !_isLoading ? FlatButton(
                          onPressed:()  {
                            updateUserInfo(nameController.text,
                                ageController.text, phoneController.text, _userDetails.birthday, emailController.text, passwordController.text);
                          },
                          child: Text('Mettre à jour',
                            style: TextStyle(
                                color: Colors.white
                            ),
                          ),
                        ) : Container(),
                      ),
                    ],
                  ) : Container(),
                ),
              )
            ],
          ),
        )
    ));
  }

  void _showDialog(String title, String content, int goBackOnClose) {
    // flutter defined function
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Fermer"),
              onPressed: () {
                int count = 0;
                Navigator.of(context).popUntil((_) => count++ >= goBackOnClose);
              },
            ),
          ],
        );
      },
    );
  }
}
