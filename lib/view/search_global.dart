import 'package:annualproject/view/search_document.dart';
import 'package:annualproject/view/search_internet.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http_logger/log_level.dart';
import 'package:http_logger/logging_middleware.dart';
import 'package:http_middleware/http_with_middleware.dart';

class SearchDetails extends StatefulWidget {
  final herotag;

  SearchDetails({this.herotag});
  @override
  _SearchDetailsState createState() => _SearchDetailsState();
}


class _SearchDetailsState extends State<SearchDetails> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context)  {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: Color(0xFF7A9BEE),
          bottom: TabBar(
            indicator: BoxDecoration(
              borderRadius: BorderRadius.circular(23.0),
                color: Color(0xFF4563DB)),
            tabs: [
              Tab(
                  text: "Analyser mes documents",
              ),
              Tab(text: "Télécharger"),
            ],
          ),
        title: Text('Recherche & Analyse',
          style: TextStyle(
            color: Colors.white,
            fontSize: 17,
            fontFamily: "Montserrat",
          ),
         ),
        ),
        body:
        TabBarView(
          children: [
            DocumentSearchWidget(),
            InternetSearchWidget(),
          ],
        ),
      ),
    );
  }
}






