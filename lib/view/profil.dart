import 'package:annualproject/model/UserDetails.dart';
import 'package:annualproject/service/profilService.dart';
import 'package:flutter/material.dart';
import 'package:annualproject/ui/profileWidget.dart';

class ProfileDetails extends StatefulWidget {
  @override
  _ProfileDetailState createState() => _ProfileDetailState();
}


class _ProfileDetailState extends State<ProfileDetails> {
  UserDetails _userDetails;
  @override
  void initState() {
    super.initState();
    getUserDetails().then((value) => {
      setState((){
        _userDetails = value;
      })
    });
  }

  String formatDate(DateTime date){
    var day = date.day < 10 ?  '0' + date.day.toString() : date.day.toString();
    var month = date.month < 10 ?  '0' + date.month.toString() : date.month.toString();
    return  day + "-" + month + "-" +
        date.year.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFF4563DB),
        //App Bar Back Button
        title: Text("Mon Profil",
          style: TextStyle(
            color: Colors.white,
            fontSize: 17,
            fontFamily: "Montserrat",
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          // Profile Image Widget
          ProfileImageWidget(),
        ],
      ),
      body: Container(
        padding: EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
        color: Colors.white,
        width: double.infinity,
        height: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // Header Widget
            ProfileHeaderWidget(),
            SizedBox(
              height: 35.0,
            ),
            _userDetails != null ?
            Expanded(
              child: GridView.count(
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                crossAxisCount: 2,
                mainAxisSpacing: 10.0,
                crossAxisSpacing: 10.0,
                children: <Widget>[
                  ProfileCardWidget(
                    title: 'nom',
                    desc: _userDetails.name != null ? _userDetails.name : "Non précisé",
                    secondTitle: "nom d'utilisateur",
                    secondDesc: _userDetails.userName,
                    icon: Icons.people,
                    iconColor: AppTheme.iconColors[0],
                  ),
                  ProfileCardWidget(
                    title: 'age',
                    desc:  _userDetails.age != 0 ? _userDetails.age.toString() : "Non précisé",
                    icon: Icons.pages,
                    iconColor: AppTheme.iconColors[1],
                  ),
                  ProfileCardWidget(
                    title: 'email',
                    desc:  _userDetails.email,
                    icon: Icons.email,
                    iconColor: AppTheme.iconColors[2],
                  ),
                  ProfileCardWidget(
                    title: 'telephone',
                    desc:  _userDetails.phone != "" && _userDetails.phone != null
                        ? _userDetails.phone : "Non précisé",
                    icon: Icons.phone,
                    iconColor: AppTheme.iconColors[3],
                  ),
                  ProfileCardWidget(
                    title: 'date de naissance',
                    desc:  _userDetails.birthday != null ? formatDate(_userDetails.birthday) : "Non précisé",
                    icon: Icons.broken_image,
                    iconColor: AppTheme.iconColors[4],
                  ),

                  // Add New To do Card
                  ProfileAddCardWidget(),
                ],
              ),
            ): Container(),
          ],
        ),
      ),
    );
  }
}







