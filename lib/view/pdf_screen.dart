import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';

class PDFScreen extends StatelessWidget {
  String pathPDF = "";
  PDFScreen(this.pathPDF);

  @override
  Widget build(BuildContext context) {
    return PDFViewerScaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF4563DB),
          title: Text("Document",
            style: TextStyle(
            color: Colors.white,
            fontSize: 17,
            fontFamily: "Montserrat ",
          ),),
        ),
        path: pathPDF);
  }
}
