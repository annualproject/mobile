import 'dart:async';
import 'dart:convert';

import 'package:annualproject/service/documentSetService.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http_middleware/http_middleware.dart';
import 'package:http_logger/http_logger.dart';

import 'add_document_to_folder.dart';



class DocumentSetsDetails extends StatefulWidget {
  final name;
  final herotag;

  DocumentSetsDetails({this.name, this.herotag});
  @override
  _DocumentSetsDetailsState createState() => _DocumentSetsDetailsState();
}

HttpWithMiddleware httpClient = HttpWithMiddleware.build(
    middlewares: [
  HttpLogger(logLevel: LogLevel.BODY),

]);

class _DocumentSetsDetailsState extends State<DocumentSetsDetails> {
  final _addDatasetForm = GlobalKey<FormState>();
  final datasetNameForm = TextEditingController();
  List _dataset;
  var _datasetField = TextEditingController();
  var isLoading = true;

  @override
  void initState() {
    super.initState();
    getDatasetOfUser().then((result) {
      setState(() {
        _dataset = result;
      });
    }).whenComplete((){
      setState(() {
        isLoading = false;
      });
    });
  }

  Future<void> refreshDataSet() async{
    var result = await getDatasetOfUser();
    setState(() {
      _dataset = result;
    });
  }

  addDataset(String name) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map data = {
      'name': name,
    };
    var response;
    try{
      print(sharedPreferences.get("token"));
      response = await httpClient.post( urlApi + "documentSets",
          headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")}, body:json.encode(data));
      refreshDataSet();
    }
    catch( e ){
      print(e.toString());
      _showDialogErreur("Connexion au serveur impossible");

      return;
    }
    if(response.statusCode == 200) {
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
    }
    else if(response.statusCode == 400 || response.statusCode == 401 ) {
      var res = json.decode(response.body);
      print(res["message"]);
      _showDialogErreur(res["message"]);
    }
    else {
      print(response.body);
    }
  }

  @override
  Widget build(BuildContext context)  {

    return Scaffold(
      backgroundColor: Color(0xFF4563DB),
      appBar:  AppBar(
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          icon: Icon (Icons.arrow_back_ios),
          color: Colors.white,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text('Dossiers de documents',
          style: TextStyle(
            color: Colors.white,
            fontSize: 17,
            fontFamily: "Montserrat",
          ),

        ),centerTitle: true,
      ),
      floatingActionButtonLocation :FloatingActionButtonLocation.centerFloat,
      floatingActionButton: new FloatingActionButton.extended(
        onPressed: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Container(
                  width: 10.0,
                  height: 192.0,
                  child:Stack(
                    overflow: Overflow.visible,
                    children: <Widget>[
                      Positioned(
                        right: -40.0,
                        top: -40.0,
                        child: InkResponse(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: CircleAvatar(
                            child: Icon(Icons.close),
                            backgroundColor: Colors.grey,
                          ),
                        ),
                      ),
                      Form(
                        key: _addDatasetForm,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text("Nom du datatset"),
                            ),
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: TextField(
                                controller: _datasetField,
                              )
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: RaisedButton(
                                child: Text("Créer"),
                                onPressed: () {
                                  if (_addDatasetForm.currentState.validate()) {
                                    addDataset(_datasetField.text);
                                    _datasetField.clear();
                                    Navigator.of(context).pop();
                                  }
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            });
        },
        label: Text("Ajouter"),
        backgroundColor: Color(0xFF4563DB),
      ),
      body: ListView(
        children:[
          Stack(
            children: [
              Container(
                height:  MediaQuery.of(context).size.height - 82.0,
                width:  MediaQuery.of(context).size.width,
                color: Colors.transparent,
              ),
              Positioned(
                top: 10.0,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  height:  MediaQuery.of(context).size.height - 110.0,
                  width:  MediaQuery.of(context).size.width,
                  child: _dataset != null ?
                  new RefreshIndicator(
                    onRefresh: refreshDataSet,
                    child :  ListView.builder(
                    padding: const EdgeInsets.all(8),
                    itemCount: _dataset.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        height: 100,
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                                width: 1.0, color: Colors.grey[300]),
                          ),
                        ),
                        child: (
                          ListTile(
                            contentPadding: EdgeInsets.all(16.0),
                            leading: SizedBox(
                                height: 100.0,
                                width: 100.0, // fixed width and height
                                child: Image.asset("assets/folder.png")
                            ),
                            trailing: Icon(Icons.arrow_forward_ios),
                            title: Text('${_dataset[index]["name"]}',
                                  style: TextStyle( fontSize: 18),
                                ),
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute
                              (builder: (BuildContext context) =>
                              AddDocumentToFolder('${_dataset[index]["uuid"]}'),
                            ));
                            },
                          )
                        )
                      );
                    }
                  )
                  ):
                  Text("Vous n'avez aucun dossier de document"),
                ),
              ),
              Positioned(
                top: 30,
                left:  MediaQuery.of(context).size.width/2 - 50.0,
                child: Hero(
                  tag: widget.herotag,
                  child: Container(
                    height: 100,
                    width: 100,
                  ),
                ),
              )
            ],
          )
        ],
      )
    );
  }

  void _showDialogErreur(String text) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Une erreur s'est produite"),
          content: new Text(text),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
