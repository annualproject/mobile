import 'package:annualproject/service/authService.dart';
import 'package:flutter/material.dart';
import 'package:loading_animations/loading_animations.dart';


import 'home.dart';


class Loginpage extends StatefulWidget {
  @override
  _LoginpageState createState() => _LoginpageState();
}
final TextEditingController usernameController = new TextEditingController();
final TextEditingController passwordController = new TextEditingController();


class _LoginpageState extends State<Loginpage> {
  bool _isLoading = false;

  @override
  void initState() {

    //Commented because it block the use of android emulator
    //SystemChrome.setEnabledSystemUIOverlays([]);
    super.initState();
  }

  signIn(String username, String password) async {

    if(username == "" || password == ""){
      _showDialog("Champs vides", "Veuillez remplir tous les champs");
      return;
    }

    setState(() {
      _isLoading = true;
    });

    var response = await auth(username, password);

    if(response == 500){
      _showDialog("Oups", "Une erreur s'est produite");
    }
    else if(response == 200){
      setState(() {
        _isLoading = false;
      });

      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => HomePage()),
              (Route<dynamic> route) => false);
    }
    else if(response == 403){
      _showDialog("", "Vos identifiants sont incorrects");
    }

    setState(() {
      _isLoading = false;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          icon: Icon (Icons.arrow_back_ios),
          color: Colors.purple,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,

      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Stack(
          children: <Widget>[
            Positioned(
              child: Image.asset(
                "assets/onboarding2.png",
              ),
            ),

            Positioned(
              top: 100,
              left: 32,
              child: Text('Connexion',
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white
                ),
              ),
            ),

            Positioned(
              top: 190,
              child: Container(
                padding: EdgeInsets.all(32),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(62),
                        topRight: Radius.circular(62)
                    )
                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 16, bottom: 0),
                      child: TextFormField(
                        controller: usernameController,
                        decoration: InputDecoration(
                            hintText: 'Nom d\'utilisateur',
                          icon: const Icon(Icons.supervised_user_circle),
                        ),

                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.only(top: 16, bottom: 62),
                      child: TextFormField(
                        controller: passwordController,
                        decoration: InputDecoration(
                            hintText: 'Mot de passe',
                          icon: const Icon(Icons.lock),

                        ),
                        obscureText: true,
                      ),
                    ),

                    !_isLoading ?
                    Container(
                      height: 45,
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                        color: Colors.purple,
                        borderRadius: BorderRadius.all(
                            Radius.circular(32)
                        ),
                      ),
                      child:  FlatButton(
                        onPressed:()  {
                          signIn(usernameController.text, passwordController.text);
                        },
                        child: Text('SE CONNECTER',
                          style: TextStyle(
                              color: Colors.white
                          ),
                        ),
                      ),
                    ):
                    LoadingRotating.square(
                      borderColor: Colors.lightBlue,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _showDialog(String title, String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(title),
          content: new Text(content),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Fermer"),
              onPressed: () {
                usernameController.clear();
                passwordController.clear();

                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

