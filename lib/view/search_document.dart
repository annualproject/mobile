import 'dart:async';

import 'package:annualproject/service/documentSetService.dart';
import 'package:annualproject/service/taskAnalyseService.dart';
import 'package:annualproject/view/synthesis.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class DocumentSearchWidget extends StatefulWidget {

  @override
  DocumentSearchState createState() => DocumentSearchState();
}


class DocumentSearchState extends State<DocumentSearchWidget> {

  List _documentSet = [];
  var _searchQuery = TextEditingController();
  bool analysisIsLaunch = false;
  var barProgression = 0.0;
  var locationTaskAnalyseURL = "";
  bool taskToReload = false;

  bool summaryChecked = false;
  var _summarySize = 1;
  var task;
  var selectedDocumentSet;

  @override
  State<StatefulWidget> createState() {}

  @override
  void initState() {
    super.initState();
    reloadTaskNotFinished();
    getDatasetOfUser().then((result) {
      setState(() {
        _documentSet = result;
      });
    });
  }

  void reloadTaskNotFinished(){
    const oneSec = const Duration(seconds:10);
    new Timer.periodic(oneSec, (Timer t) => {
      if(taskToReload){
        refreshTaskAnalyse()
      }
    });
  }

  Future<void> refreshTaskAnalyse() async{
    if(locationTaskAnalyseURL != "" && mounted){
      var currentTask = await getCurrentTaskAnalyse(locationTaskAnalyseURL);

      setState(() {
        task = currentTask;
      });

      setState(() {
        switch (currentTask["taskStatus"]){
          case "TO_BEGIN":
            barProgression = 0.35;
            taskToReload = true;
            break;
          case "IN_PROGRESS":
            barProgression = 0.75;
            taskToReload = true;
            break;
          case "FINISHED":
            barProgression = 0.999;
            taskToReload = false;
            break;
          default:
            taskToReload = false;
            barProgression = 0;
            break;
        }
      });
    }
  }



  launchResearch(String searchQuery, selectedDocumentSet, bool summaryChecked, int summarySize) async {

    var taskAnalyseUrl = await addTaskAnalyse(searchQuery, selectedDocumentSet, summaryChecked, summarySize);

    if(taskAnalyseUrl == 500){
      _showDialog("Erreur", "Une erreur s'est produite");
    }
    else{
      _showDialog("", "Votre recherche " + searchQuery + " est en cours de traitement sur le dossier " + selectedDocumentSet["name"]);
      setState(() {
        summaryChecked = false;
        summarySize = 0;
      });
      locationTaskAnalyseURL = taskAnalyseUrl;
      taskToReload = true;

    }
  }


  Future<void> refreshDataSet() async{
    var result = await getDatasetOfUser();
    setState(() {
      _documentSet = result;
    });
  }

  Future _showNumberPicker() async {
    await showDialog<int>(
      context: context,
      builder: (BuildContext context) {
        return new NumberPickerDialog.integer(
          minValue: 0,
          maxValue: 1000,
          initialIntegerValue: _summarySize,
          cancelWidget: Text("Annuler"),
          confirmWidget: Text("Ok"),
        );
      },
    ).then((num value) {
      if (value != null) {
        setState(() => _summarySize = value);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return
    SingleChildScrollView(
      child:
      Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            Container(
              margin: EdgeInsets.only(top: 8.0, bottom: 15.0),
              width: double.infinity,
              color: Colors.grey[200],
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 8.0),
                    child: Text("Séléctionnez votre dossier", style: TextStyle(fontStyle: FontStyle.italic, fontSize: 15.0, color: Color(0xFF4563DB))),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top:8.0, left: 25.0),
                    child: _documentSet != null ? DropdownButton(
                      iconEnabledColor: Colors.blue,
                      hint:  Text("Un dossier"),
                      value: selectedDocumentSet,
                      onChanged: (Value) {
                        setState(() {
                          selectedDocumentSet = Value;
                        });
                      },
                      items:
                      _documentSet.map((set) {
                        return  DropdownMenuItem(
                          value: set,
                          child: Row(
                            children: <Widget>[
                              //SizedBox(width: 10,),
                              Text(
                                set["name"],
                                style:  TextStyle(color: Colors.black),
                              ),
                            ],
                          ),
                        );
                      }).toList(),
                    ): Text("Vous n'avez enregistré aucun document"),
                  ),
                ],
              ),
            ),

            Container(
                margin: EdgeInsets.only(bottom: 15.0),
                width: double.infinity,
                color: Colors.grey[200],
                child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top:8.0),

                        child: Text("Entrez une phrase ou un mot clé", style: TextStyle(fontStyle: FontStyle.italic, fontSize: 15.0, color: Color(0xFF4563DB))),
                      ),
                      Padding(
                          padding: EdgeInsets.all(25.0),
                          child:
                          Container(
                              height: 5.0 * 24.0,
                              child:TextField(
                                maxLines: 5,
                                controller: _searchQuery,
                                decoration: InputDecoration(
                                  hintText: "Kennedy...",
                                  fillColor: Colors.white,
                                  filled: true,
                                ),
                              )
                          )


                      ),
                    ])
            ),
            Container(
                margin: EdgeInsets.only( bottom: 15.0),
                width: double.infinity,
                color: Colors.grey[200],
                child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text("Résumé", style: TextStyle(fontStyle: FontStyle.italic, fontSize: 15.0, color: Color(0xFF4563DB))),
                      ),
                      !analysisIsLaunch ?
                      Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: CheckboxListTile(
                              title: Text('Faire un résumé des documents',
                                  style: TextStyle( fontSize: 15.0)),
                              value: summaryChecked,
                              checkColor: Colors.white,
                              activeColor: Color(0xFF4563DB),
                              onChanged: (bool value) {
                                setState(() {
                                  summaryChecked = value;
                                });
                              },
                              controlAffinity: ListTileControlAffinity.leading
                          )
                      ):
                      Container(),
                      summaryChecked && !analysisIsLaunch ?
                      Padding(
                          padding: EdgeInsets.all(0.0),
                          child:Row(

                              children: [
                                Padding(
                                    padding:EdgeInsets.only(left: 25.0, top: 10.0),
                                    child:Text("Résumé de ",
                                        style: TextStyle(fontSize: 15.0))
                                ),
                                RaisedButton(
                                  color: Color(0xFF4563DB),
                                  textColor: Colors.white,
                                  child: Text(_summarySize.toString()),
                                  onPressed: (){
                                    _showNumberPicker();
                                  },
                                ),
                                Padding(
                                    padding:EdgeInsets.only(left: 10.0, top: 10.0),
                                    child:Text("phrase(s)", style: TextStyle(fontSize: 15.0))
                                )
                              ]
                          )
                      )
                          : Container(),
                      Padding(
                          padding: EdgeInsets.all(25.0),
                          child:
                          Row(
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: Icon(
                                      Icons.info
                                  )
                              ),
                              Flexible(
                                  child: !summaryChecked ?
                                  Text("Votre résultat contiendra toutes les phrases contenant votre recherche",
                                      style: TextStyle( fontSize: 15.0))
                                      :
                                  Text("Votre résultat sera un résumé des documents contenant votre recherche",
                                      style: TextStyle( fontSize: 15.0))
                              )
                            ],
                          )

                      ),
                    ])),

            ! analysisIsLaunch ?
            Center(
              child: RaisedButton(
                color: Color(0xFF4563DB),
                textColor: Colors.white,
                child: Text("Lancer l'analyse"),
                onPressed: (){
                  if(_searchQuery.value != null && selectedDocumentSet != null && _searchQuery.text != "") {
                    launchResearch(_searchQuery.text, selectedDocumentSet, summaryChecked, _summarySize);
                    setState(() {
                      analysisIsLaunch = true;
                    });
                  }
                  else{
                    _showDialog("", "Vous devez remplir tous les champs");
                  }
                },
              ),
            ):
            Center(
              child: RaisedButton(
                color: Colors.grey,
                textColor: Colors.white,
                child: Text("Lancer l'analyse"),
              ),
            )
            ,
            analysisIsLaunch ?
            Padding(
              padding: EdgeInsets.only(left: 25.0, top: 8.0, bottom: 8.0),
              child:
              LinearPercentIndicator(
                width: 350.0,
                lineHeight: 14.0,
                percent: barProgression,
                linearStrokeCap: LinearStrokeCap.roundAll,
                backgroundColor: Color(0xFF7A9BEE),
                progressColor:  Color(0xFF4563DB),
              ),
            ): Container(),
            analysisIsLaunch && barProgression == 0.999 ?
            Center(
              child: RaisedButton(
                color: Color(0xFF4563DB),
                textColor: Colors.white,
                child: Text("Consulter le résultat"),
                onPressed: (){
                  Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (context)=> SynthesisDetails(selectedDocumentSet["uuid"],
                              selectedDocumentSet["name"], _searchQuery.text,
                              task["synthesis"], true)
                      )
                  );
                },
              ),
            )
                : Container(),
          ],
      )
    );
  }

  void _showDialog(String title, String content) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Fermer"),
              onPressed: () {

                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}