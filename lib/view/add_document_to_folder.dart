import 'package:annualproject/service/documentService.dart';
import 'package:annualproject/service/documentSetService.dart';
import 'package:annualproject/view/pdf_screen.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:imagebutton/imagebutton.dart';
import 'package:permission_handler/permission_handler.dart';


class AddDocumentToFolder extends StatefulWidget {
  final String documentSetUuid;


  AddDocumentToFolder(this.documentSetUuid);

  @override
  _AddDocumentToFolderState createState() => new _AddDocumentToFolderState();
}

class _AddDocumentToFolderState extends State<AddDocumentToFolder> {
  var _dataset;
  String _url = "";
  String _fileName = '...';
  String _extension;
  bool _hasValidMime = false;
  FileType _typefile;
  String documentSetUuid;
  var isLoading = true;
  String pathTo;

  @override
  void initState() {
    super.initState();

    documentSetUuid = widget.documentSetUuid;
    getDocumentsOfDocumentSet(documentSetUuid).then((result) {
      setState(() {
        _dataset = result;
      });
    }).whenComplete((){
      setState(() {
        isLoading = false;
      });
    });
  }
  
  void _openFile() async {

    if (_typefile != FileType.CUSTOM || _hasValidMime) {
      try {
        _url = await FilePicker.getFilePath(
            type: _typefile, fileExtension: _extension);

        if(_url == null) return;
        if(!mounted) return;
        setState(() {
          _fileName = _url != null ? _url
              .split('/')
              .last : '...';
        });

        var status = await Permission.storage.status;
        if (!status.isGranted) {
          await Permission.storage.request();
          var uploadResult = await uploadfile(_url, documentSetUuid);

          if(uploadResult == 201){
            _showDialog("Envoyé !", "Votre document à été ajouté à votre dossier");
            refreshDataSet();
          }else{
            _showDialog("Oups", "Une erreur s'est produite");
          }
        }
        else{
          var uploadResult = await uploadfile(_url, documentSetUuid);

          if(uploadResult == 201){
            _showDialog("Envoyé !", "Votre document à été ajouté à votre dossier");
            refreshDataSet();
          }
          else{
            _showDialog("Oups", "Une erreur s'est produite");
          }
        }
        
      } catch (e) {
        print("Unsupported operation" + e.toString());
      }
    }
  }

  void _showDialog(String title, String content) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(content),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Fermer"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> refreshDataSet() async{
    var result = await getDocumentsOfDocumentSet(documentSetUuid);
    setState(() {
      _dataset = result;
    });
  }



  @override
  Widget build(BuildContext context) {
    if(isLoading){_dataset = [{"name":"Empty"}];}
    if(!isLoading && _dataset ==  null){_dataset = [{"name":"Empty"}];}
    return Scaffold(
      appBar: AppBar(title: const Text('Mes Documents',
        style: TextStyle(
          color: Colors.white,
          fontSize: 17,
          fontFamily: "Montserrat ",
        ),
      ),
        elevation: 0.0,
        backgroundColor: Color(0xFF4563DB),
      ),
      body: Column(
        children:[
          ImageButton(
            children: <Widget>[],
            width: 91,
            height: 76,
            paddingTop: 5,
            pressedImage: Image.asset(
              "assets/addDoc.png",
                color: Colors.lightBlue
            ),
            unpressedImage: Image.asset("assets/addDoc.png"),
            onTap: () => _openFile(),
          ),
          Expanded(
            child: _dataset.length != 0 ?
          RefreshIndicator(
              onRefresh: refreshDataSet,
              child : ListView.builder(
                  shrinkWrap: true,
                  padding: const EdgeInsets.all(8),
                  itemCount: _dataset.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Dismissible(
                      background: slideRightBackground(),
                      secondaryBackground: slideLeftBackground(),
                      key: Key (_dataset[index]["uuid"]),
                       child: Row(
                           children: <Widget>[
                             ImageButton(
                               children: <Widget>[],
                               width: 91,
                               height: 76,
                               paddingTop: 5,
                               pressedImage: Image.asset(
                                 "assets/file.png",
                               ),
                               unpressedImage: Image.asset("assets/file.png"),
                               onTap: () async {

                                 pathTo =  await downloadFile(_dataset[index]["name"],_dataset[index]["uuid"]);
                                 if(pathTo != 500){
                                   Navigator.push(context, MaterialPageRoute(
                                     builder: (context)=>
                                         PDFScreen(pathTo),
                                   ));
                                 }
                                 else{
                                   _showDialog("Oups", "Impossible d'ouvrir le fichier");
                                 }
                              },
                             ),
                             Text('${_dataset[index]["name"]}',
                                 style: TextStyle( fontSize: 10)),
                             SizedBox(
                               height: 5,
                             ),
                           ]
                       ),

                      // ignore: missing_return
                      confirmDismiss: (direction) async {
                        if (direction == DismissDirection.endToStart) {
                          final bool res = await showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  content: Text(
                                      "Etes vous sûr de vouloir supprimer ${_dataset[index]["name"]}?"),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text(
                                        "Annuler",
                                        style: TextStyle(color: Colors.black),
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                    FlatButton(
                                      child: Text(
                                        "Supprimer",
                                        style: TextStyle(color: Colors.red),
                                      ),
                                      onPressed: () {
                                        deleteDocument(documentSetUuid, "${_dataset[index]["uuid"]}");
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ],
                                );
                              });
                          return res;
                        }
                      },
                    );
                  }
              ),
          ):
          Padding(
            padding: EdgeInsets.all(12.0),
            child: Text("Ce dossier ne contient aucun document, ajoutez-en un !"),
          )

          )
        ],
      ),
    );
  }
}
Widget slideLeftBackground() {
  return Container(
    color: Colors.red,
    child: Align(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Icon(
            Icons.delete,
            color: Colors.white,
          ),
          Text("Supprimer", style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.right,
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
      alignment: Alignment.centerRight,
    ),
  );
}
Widget slideRightBackground() {
  return Container(
    color: Colors.green,
    child: Align(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            width: 20,
          ),
          Icon(
            Icons.edit,
            color: Colors.white,
          ),
          Text(
            "Editer",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.left,
          ),
        ],
      ),
      alignment: Alignment.centerLeft,
    ),
  );
}




