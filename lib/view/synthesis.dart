import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SynthesisDetails extends StatefulWidget {
  final String documentSetUuid;
  final String documentSetName;
  final String queryString;
  final String taskAnalyseSynthesis;
  final bool goBackToMenu;

  SynthesisDetails(this.documentSetUuid, this.documentSetName, this.queryString, this.taskAnalyseSynthesis, this.goBackToMenu);
  @override
  _SynthesisDetailsState createState() => _SynthesisDetailsState();
}


class _SynthesisDetailsState extends State<SynthesisDetails> {
  String documentSetUuid;
  String documentSetName;
  String queryString;
  String taskAnalyseSynthesis;
  bool goBackToMenu;

  @override
  void initState() {
    super.initState();
    documentSetUuid = widget.documentSetUuid;
    documentSetName = widget.documentSetName;
    queryString = widget.queryString;
    taskAnalyseSynthesis = widget.taskAnalyseSynthesis;
    goBackToMenu = widget.goBackToMenu;
  }


  @override
  Widget build(BuildContext context)  {

    return Scaffold(
      backgroundColor: Colors.white,
      appBar:  AppBar(
        backgroundColor: Color(0xFF4563DB),
        elevation: 0.0,
        title: Text('Synthèse d\'analyse',
          style: TextStyle(
            color: Colors.white,
            fontSize: 17,
            fontFamily: "Montserrat",
          ),
        ),centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            int count = 0;
            int nbScreens = goBackToMenu == true ? 2 : 1;
            Navigator.of(context).popUntil((_) => count++ >= nbScreens);
        }),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(bottom: 8.0),
                child : Text(documentSetName, style: TextStyle(fontSize: 20.0))
            ),
            Padding(
                padding: EdgeInsets.only(bottom: 8.0),
                child : Text(queryString, style: TextStyle(fontSize: 17.0, color: Colors.grey))
            ),
            taskAnalyseSynthesis != "" ?
            Text(taskAnalyseSynthesis):
            Text("L'application n'a trouvé aucun résultat correspondant à votre recherche.")
          ])
        )
    );
  }

  void _showDialogErreur(String text) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Une erreur s'est produite"),
          content: new Text(text),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Fermer"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
