import 'dart:async';
import 'dart:core';

import 'package:annualproject/model/HistoryItem.dart';
import 'package:annualproject/service/documentSetService.dart';
import 'package:annualproject/service/taskAnalyseService.dart';
import 'package:annualproject/view/synthesis.dart';
import 'package:flutter/material.dart';
import 'package:loading_animations/loading_animations.dart';

class HistoriqueDetails extends StatefulWidget {
  final herotag;
  final name;

  HistoriqueDetails({this.herotag, this.name});
  @override
  _HistoriqueDetailsState createState() => _HistoriqueDetailsState();
}

class _HistoriqueDetailsState extends State<HistoriqueDetails> {

  String name;
  List _analysisHistory = new List<HistoryItem>();
  bool _isLoading = true;
  bool taskToReload = false;

  @override
  void initState() {
    super.initState();
    name = widget.name;
    reloadTaskNotFinished();
    getAnalysisHistoryOfUser().then((resultWithoutLength) {
      if(resultWithoutLength != null) {
        addDocumentSetLength(resultWithoutLength).then((resultWithoutName) {

          addDocumentSetName(resultWithoutName).then((resultComplete){

            setState(() {
              _analysisHistory = resultComplete;
              _isLoading = false;
            });
          });
        });
      }
    });
  }


  void reloadTaskNotFinished(){
    const oneSec = const Duration(seconds:10);
    new Timer.periodic(oneSec, (Timer t) => {
      if(taskToReload){
        refreshAnalysisHistory()
      }
    });
  }

  String convertStatus(String taskStatus){
    switch (taskStatus){
      case 'TO_BEGIN':
        taskToReload = true;
        return "Non commencée";
      case 'IN_PROGRESS':
        taskToReload = true;
        return "En cours";
      case 'FINISHED':
        return "Terminée";
      default: return "En erreur";
    }
  }

  Future<List<HistoryItem>> addDocumentSetName(List<HistoryItem> analysisHistory) async {
    for (HistoryItem analyse in analysisHistory) {
      var documentSet = await getDocumentSetByUuid(analyse.documentSetUuid);
      if(documentSet == 500){
        _showDialog("Erreur", "Une erreur s'est produite");
      }
      else{
        analyse.documentSetName = documentSet["name"];
      }
    }
    return analysisHistory;
  }

  Future<List<HistoryItem>> addDocumentSetLength(List<HistoryItem> analysisHistory) async {
    for(HistoryItem analyse in analysisHistory) {

      var documents = await getDocumentsOfDocumentSet(analyse.documentSetUuid);
      if(documents == 500){
        _showDialog("Erreur", "Une erreur s'est produite");
      }
      else{
        analyse.documentSetLength = documents.length;
      }
    }

    return analysisHistory;
  }

  Future<List<HistoryItem>> getAnalysisHistoryOfUser() async {
    List<HistoryItem> taskList = new List<HistoryItem>();
    var taskAnalysis = await getTaskAnalysisOfUser();

    if(taskAnalysis == 500){
      _showDialog("Erreur", "Une erreur s'est produite");
    }
    else{
      for (var res in taskAnalysis){
        taskList.add(new HistoryItem(res["documentSetUuid"], "", 0, res["queryString"], res["taskStatus"], res["synthesis"]));
      }
    }

    return taskList;
  }

  Future<void> refreshAnalysisHistory() async{

    if(mounted){
      var resultWithoutLength = await getAnalysisHistoryOfUser();
      var resultWithoutName = await addDocumentSetLength(resultWithoutLength);
      var resultComplete = await addDocumentSetName(resultWithoutName);

      setState(() {
        _analysisHistory = resultComplete;
        taskToReload = false;
      });
    }
  }

  @override
  Widget build(BuildContext context)  {
    return Scaffold(
      backgroundColor: Color(0xFF4563DB),
      appBar:  AppBar(
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          icon: Icon (Icons.arrow_back_ios),
          color: Colors.white,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text(name,
          style: TextStyle(
            color: Colors.white,
            fontSize: 17,
            fontFamily: "Montserrat",
          ),
        ),centerTitle: true,
      ),
      body: ListView(
        children:[
          Stack(
            children: [
              Container(
                height:  MediaQuery.of(context).size.height - 82.0,
                width:  MediaQuery.of(context).size.width,
                color: Colors.transparent,
              ),
              Positioned(
                top: 10.0,
                child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    height:  MediaQuery.of(context).size.height - 110.0,
                    width:  MediaQuery.of(context).size.width,
                    child: !_isLoading ?
                    new RefreshIndicator(
                        onRefresh: refreshAnalysisHistory,
                        child : _analysisHistory.length != 0 ? ListView.builder(
                            padding: const EdgeInsets.all(8),
                            itemCount: _analysisHistory.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                  height: 70,
                                  decoration:
                                  _analysisHistory[index].taskStatus == 'FINISHED' ?
                                  BoxDecoration(
                                    border: Border(
                                      bottom: BorderSide(
                                          width: 1.0, color: Color(0xFF7A9BEE)),
                                    ),
                                  ) :
                                  BoxDecoration(
                                    color: Color(0xFFE1E4E4),
                                    border: Border(
                                      bottom: BorderSide(
                                          width: 1.0, color:Color(0xFF7A9BEE)),
                                    ),
                                  )
                                  ,
                                  child: Padding(
                                      padding: EdgeInsets.all(2.0),
                                      child :
                                        ListTile(
                                          title: Text(
                                              '${_analysisHistory[index].documentSetName} ' +
                                              '(${_analysisHistory[index].documentSetLength} docs)',
                                              style: TextStyle(height: 1, fontSize: 16)
                                          ),
                                          subtitle:
                                              Text(
                                                  'Recherche: ${_analysisHistory[index].queryString}',
                                                  style: TextStyle(height: 1, fontSize: 15)
                                              ),
                                          trailing: Stack(
                                            children: <Widget>[
                                              Text("Statut de l'analyse",
                                              style: TextStyle(color: Color(0xFF4563DB), )),
                                              Padding(
                                                child:
                                                Text(
                                                  convertStatus(_analysisHistory[index].taskStatus),
                                                  style: TextStyle(height: 1, fontSize: 15)),
                                                padding: EdgeInsets.only(top: 18.0),
                                              ),
                                            ],
                                          ),
                                          isThreeLine: true,
                                          onTap: () {
                                            if(_analysisHistory[index].taskStatus.toString() == 'FINISHED'){
                                              Navigator.of(context).push(
                                                  MaterialPageRoute(
                                                      builder: (context)=> SynthesisDetails(_analysisHistory[index].documentSetUuid,
                                                          _analysisHistory[index].documentSetName, _analysisHistory[index].queryString,
                                                          _analysisHistory[index].taskSynthesis, false)
                                                  )
                                              );
                                            }
                                          },
                                        )
                                      )
                              );
                            }
                        ):
                        Padding(child: Text("Vous n'avez effectué aucune recherche"),
                          padding: EdgeInsets.all(12.0),),
                    ):
                    LoadingRotating.square(
                      borderColor: Colors.lightBlue,
                  ),
                ),
              ),
              Positioned(
                top: 30,
                left:  MediaQuery.of(context).size.width/2 - 50.0,
                child: Hero(
                  tag: widget.herotag,
                  child: Container(
                    height: 100,
                    width: 100,
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  void _showDialog(String title, String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(content),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Fermer"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
