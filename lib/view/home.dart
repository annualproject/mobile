import 'package:annualproject/view/search_global.dart';
import 'package:flutter/material.dart';
import 'package:annualproject/view/profil.dart';
import 'package:annualproject/view/historique.dart';
import 'package:annualproject/view/documentSets.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();

}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF4563DB),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 15.0, left: 13.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: 125.0,
                  child: Row(
                    mainAxisAlignment:  MainAxisAlignment.spaceBetween,
                  ),
                ),
              ],
            ),
          ),
          SizedBox( height: 25.0),
          Padding(
            padding: EdgeInsets.only(left: 40.0),
            child: Row(
              children: <Widget>[
                Text("InfoSearch",
                  style:TextStyle (
                    color: Colors.white,
                    fontSize: 28,
                    fontFamily: "Montserrat"
                  ),
                )
              ],
            ),
          ),
          SizedBox( height: 40.0),
          Container(
            height: MediaQuery.of(context).size.height - 185.0 ,
              decoration: (
              BoxDecoration(
                color: Colors.white,
              )
              ),
              child: ListView(
                primary: false,
                padding: EdgeInsets.only(left: 25.0, right: 25),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 45.0),
                  child: Container(
                    height: MediaQuery.of(context).size.height - 300.0 ,
                    child: ListView(
                      children: <Widget>[
                          build_Item("assets/search.png", "Recherche", 1),
                          build_Item("assets/document.png", "Mes documents", 2),
                          build_Item("assets/dode.png", "Historique", 3),
                          build_Item("assets/prof.png", "Profil", 4),
                      ],
                    ),
                  ),
                )
              ],)
          ),
        ],
      ),
    );
  }
    Widget build_Item(String imagePath, String name, int i){
      return Padding(
        padding: EdgeInsets.only(top: 10.0, right: 10, left: 10),
        child:InkWell(
          onTap: (){
            print(name);
            if(i == 1){
              Navigator.of(context).push(MaterialPageRoute
                (builder: (BuildContext context) =>
                  SearchDetails(herotag: imagePath)
              ));
              //(Route<dynamic> route) => false);
            }
            if(i == 2){
              Navigator.of(context).push(MaterialPageRoute
                (builder: (BuildContext context) =>
                  DocumentSetsDetails(herotag: imagePath ,name: name)
              ));
              //(Route<dynamic> route) => false);
            }
            if(i == 3){
              Navigator.of(context).push(MaterialPageRoute
                (builder: (BuildContext context) =>
                  HistoriqueDetails(herotag: imagePath, name: 'Historique')
              ));
              //(Route<dynamic> route) => false);

            }
            if(i == 4){
              Navigator.of(context).push(MaterialPageRoute
                (builder: (BuildContext context) =>
                  ProfileDetails()
              ));
              //(Route<dynamic> route) => false);
            }
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Row(
                  children:[
                    Hero(
                      tag : imagePath,
                      child: Image(
                        image: AssetImage(imagePath),
                        fit: BoxFit.cover,
                        height: 100,
                        width: 100,
                      ),
                    ),
                    SizedBox(width: 40 ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          name,
                          style:  TextStyle(
                              fontSize: 17,
                              fontFamily: "Montserrat",
                              fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
              IconButton(
                icon: Icon(Icons.arrow_right),
                color: Colors.black
              ),
            ],
          ),
        ) ,
      );
  }
}
