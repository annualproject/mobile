import 'dart:async';

import 'package:annualproject/service/taskSearchService.dart';
import 'package:annualproject/view/documentSets.dart';
import 'package:flutter/material.dart';


class InternetSearchWidget extends StatefulWidget {

  @override
  InternetSearchState createState() => InternetSearchState();
}

class InternetSearchState extends State<InternetSearchWidget>{

  List _searchHistory = [];
  var isLoading = false;
  var _searchQuery = TextEditingController();
  bool taskSearchToReload = false;


  @override
  void initState() {
    super.initState();
    //refresh if a task is not finished
    reloadTaskNotFinished();
    getSearchHistoryOfUser().then((result) {
      setState(() {
        _searchHistory = result;
      });
    }).whenComplete((){
      setState(() {
        isLoading = false;
      });
    });
  }


  void reloadTaskNotFinished(){
    const oneSec = const Duration(seconds:10);
    new Timer.periodic(oneSec, (Timer t)  {
      if(taskSearchToReload == true){
        refreshSearchHistory();
      }
    });
  }

  String convertStatus(String taskStatus){
    switch (taskStatus){
      case 'TO_BEGIN':
        taskSearchToReload = true;
        return "Non commencé";
      case 'IN_PROGRESS':
        taskSearchToReload = true;
        return "En cours";
      case 'FINISHED':
        return "Terminé";
      default:
        return "En erreur";
    }
  }



  Future<void> refreshSearchHistory() async{
    if(mounted){
      var result = await getSearchHistoryOfUser();

      setState(() {
        _searchHistory = result;
        taskSearchToReload = false;
      });
    }
  }

  launchResearch(String searchQuery) async {

    var result = await addTaskSearch(searchQuery);
    if(result == 200){
      refreshSearchHistory();
      _showDialog("", "Votre recherche " + searchQuery + " est en cours de traitement...");
      _searchQuery.clear();
    }
    else if (result == 409){
      _showDialog("Erreur", "Cette recherche est déjà en cours");
    }
    else{
      _showDialog("Erreur", "Une erreur s'est produite");
    }

  }

  @override
  Widget build(BuildContext context) {
    return
      Column(
          children:<Widget>[
            Form(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 15.0, left: 25.0, right:25.0),
                        child: Text("La recherche sur internet crée un dossier de document sur lequel vous pourrez lancer une analyse",
                            style: TextStyle( fontSize: 15.0, color: Color(0xFF4563DB))),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 15.0, left: 25.0),
                        child: Text("Entrez une phrase ou un mot clé",
                            style: TextStyle(fontStyle: FontStyle.italic, fontSize: 15.0)),
                      ),
                      Padding(
                          padding: EdgeInsets.only(left:25.0, top:8.0, right:25.0, bottom:8.0),
                          child:
                          Container(
                              height: 6.0 * 24.0,
                              child:TextField(
                                maxLines: 6,
                                controller: _searchQuery,
                                decoration: InputDecoration(
                                  hintText: "Kennedy...",
                                  fillColor: Colors.grey[200],
                                  filled: true,
                                ),
                              )
                          )
                      ),
                      Center(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: RaisedButton(
                              color: Color(0xFF4563DB),
                              textColor: Colors.white,
                              child: Text("Rechercher"),
                              onPressed: (){
                                if(_searchQuery.value != null && _searchQuery.text != "") {
                                  launchResearch(_searchQuery.text);
                                }
                                else{
                                  _showDialog("", "Vous devez saisir une phrase ou un mot clé");
                                }
                              },
                            ),
                          )
                      )
                    ],
                  ),
                ),
            Expanded(
                child:
                Container(
                  child: ListView(
                    children:[
                      Padding(padding: EdgeInsets.only(left:25.0),
                        child: Text("Mes recherches (documents téléchargés)",
                        ),
                      ),
                      Stack(
                        children: [
                          Container(
                            height:  MediaQuery.of(context).size.height - 82.0,
                            width:  MediaQuery.of(context).size.width,
                            color: Colors.transparent,
                          ),
                          Positioned(
                            top: 10.0,
                            child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                ),
                                height:  MediaQuery.of(context).size.height / 3,
                                width:  MediaQuery.of(context).size.width,
                                child:  new RefreshIndicator(
                                    onRefresh: refreshSearchHistory,
                                    child :  _searchHistory != null ? ListView.builder(
                                        padding: const EdgeInsets.all(8),
                                        itemCount: _searchHistory.length,
                                        itemBuilder: (BuildContext context, int index) {
                                          return Container(
                                              height: 70,
                                              decoration:
                                              _searchHistory[index]["taskStatus"] == 'FINISHED' ?
                                                BoxDecoration(
                                                  border: Border(
                                                    bottom: BorderSide(
                                                        width: 1.0, color: Colors.grey[300]),
                                                  ),
                                                ):
                                                BoxDecoration(
                                                  color: Color(0xFFE1E4E4),
                                                  border: Border(
                                                    bottom: BorderSide(
                                                        width: 1.0, color: Colors.grey[300]),
                                                  ),
                                                ),
                                              child: Padding(
                                                  padding: EdgeInsets.all(2.0),
                                                  child :
                                                  ListTile(
                                                    title: Text(
                                                        '${_searchHistory[index]["searchQuery"]}',
                                                        style: TextStyle(height: 1, fontSize: 16)
                                                    ),
                                                    trailing: Stack(
                                                      children: <Widget>[
                                                        Text("Statut du téléchargement",
                                                            style: TextStyle(color: Color(0xFF4563DB), )),
                                                        Padding(
                                                          child:
                                                          Text(
                                                              convertStatus(_searchHistory[index]["taskStatus"]),
                                                              style: TextStyle(height: 1, fontSize: 15)),
                                                          padding: EdgeInsets.only(top: 18.0),
                                                        ),
                                                      ],
                                                    ),
                                                    onTap: () {
                                                      if(_searchHistory[index]["taskStatus"].toString() == 'FINISHED'){
                                                        Navigator.of(context).push(
                                                            MaterialPageRoute(
                                                                builder: (context)=> DocumentSetsDetails(herotag: "")
                                                            )
                                                        );
                                                      }
                                                    },
                                                  )
                                              )
                                          );
                                        }
                                    )
                                        : Text("Vous n'avez effectué aucune recherche")
                                )
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                )),
          ]
      );
  }

  void _showDialog(String title, String content) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Fermer"),
              onPressed: () {

                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}