import 'package:annualproject/service/authService.dart';
import 'package:annualproject/view/login.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:loading_animations/loading_animations.dart';

class Registerpage extends StatefulWidget {

  @override
  _RegisterpageState createState() => _RegisterpageState();
}
final TextEditingController emailController = new TextEditingController();
final TextEditingController passwordController = new TextEditingController();
final TextEditingController usernameController = new TextEditingController();


class _RegisterpageState extends State<Registerpage> {
  bool _isLoading = false;

  void signUp(String email, String username, String password) async {

    if(email == "" ||  username == "" || password == ""){
      _showDialog("Champs vides", "Veuillez remplir tous les champs");
      return;
    }

    if(!EmailValidator.validate(email)){
      _showDialog("Champs invalides", "Votre email n'est pas valide");
      return;
    }

    setState(() {
      _isLoading = true;
    });

    var response = await register(email, username, password);

    if(response == 201){
      setState(() {
        _isLoading = false;
      });
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => Loginpage()),
              (Route<dynamic> route) => false);

    }
    else if(response == 400){
      _showDialog("Erreur", "Votre saisie est invalide");
    }
    else if(response == 409){
      _showDialog("Erreur", "Ce nom d'utilisateur est déjà utilisé");
    }
    else{
      _showDialog("Oups", "Une erreur s'est produite");
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          icon: Icon (Icons.arrow_back_ios),
          color: Colors.purple,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,

      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Stack(
          children: <Widget>[
            Positioned(
              child: Image.asset(
                "assets/onboarding1.png",
              ),
            ),
            Positioned(
              top: 100,
              left: 32,
              child: Text('Inscription',
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white
                ),
              ),
            ),

            Positioned(
              top: 190,
              child: Container(
                padding: EdgeInsets.all(32),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(62),
                        topRight: Radius.circular(62)
                    )
                ),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                        controller: usernameController,
                        decoration: InputDecoration(
                            hintText: 'Nom d\'utilisateur',
                          icon: const Icon(Icons.supervised_user_circle),
                        ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 16, bottom: 0),
                      child: TextField(
                        controller: emailController,
                        decoration: InputDecoration(
                            hintText: 'Email',
                          icon: const Icon(Icons.mail),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 16, bottom: 62),
                      child: TextField(
                        controller: passwordController,
                        decoration: InputDecoration(
                            hintText: 'Mot de passe',
                          icon: const Icon(Icons.lock),
                        ),
                        obscureText: true,
                      ),
                    ),
                    !_isLoading ?
                    Container(
                      height: 45,
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                        color: Colors.purple,
                        borderRadius: BorderRadius.all(
                            Radius.circular(32)
                        ),
                      ),
                      child: FlatButton(
                        onPressed: (){
                          signUp(emailController.text,usernameController.text, passwordController.text);
                        },
                        child: Text('S\'INSCRIRE',
                          style: TextStyle(
                              color: Colors.white
                          ),
                        ),
                      ),
                    ):
                    LoadingRotating.square(
                      borderColor: Colors.lightBlue,
                    ),
                    Container(height: 8),

                    Container(height: 70),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(width: 30),
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _showDialog(String title, String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(content),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Fermer"),
              onPressed: () {
                emailController.clear();
                usernameController.clear();
                passwordController.clear();

                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

